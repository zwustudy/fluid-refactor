package com.paic.arch.jmsbroker;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.paic.arch.jmsbroker.api.ContextLoader;
import com.paic.arch.jmsbroker.api.JmsMessageBroker;
import com.paic.arch.jmsbroker.api.NoMessageReceivedException;
import com.paic.arch.jmsbroker.context.ConfigContextLoader;
import com.paic.arch.jmsbroker.context.JmsMessageBrokerContext;
import com.paic.arch.jmsbroker.impl.ActiveMQJmsMessageBroker;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 因为新重构的类JmsMessageBrokerContext严格遵循里氏替换原则继承了要重构的类JmsMessageBrokerSupport
 * 因此这里可以使用JmsMessageBrokerContext来替换JmsMessageBrokerSupport
 * @author zwustudy
 *
 */
public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerContext JMS_CONTEXT;

    @BeforeClass
    public static void setup() throws Exception {
    	ContextLoader contextLoader = new ConfigContextLoader();
		JmsMessageBroker JmsMessageBroker = new ActiveMQJmsMessageBroker(contextLoader);
		JMS_CONTEXT = new JmsMessageBrokerContext(JmsMessageBroker);
		JMS_CONTEXT.startBroker();
    }

    @AfterClass
    public static void teardown() throws Exception {
    	JMS_CONTEXT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
    	JMS_CONTEXT
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JMS_CONTEXT.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JMS_CONTEXT
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
    	JMS_CONTEXT.retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }
}
/**
 * 
 */
package com.paic.arch.jmsbroker.context;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

import com.paic.arch.jmsbroker.api.ContextLoader;

/**
 * @author zwustudy
 * 默认ContextLoader的实现，负责加载默认的jmsContext
 */
public class DefaultContextLoader implements ContextLoader {

	private static final int ONE_SECOND = 1000;
	
	protected static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    
	private static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

	@Override
	public JmsContext loadContext() {
		JmsContext defaultContext = new JmsContext();
		defaultContext.setUrl(getDefaultBrokerUrl());
		defaultContext.setUsername(null);
		defaultContext.setPassword(null);
		defaultContext.setTimeout(DEFAULT_RECEIVE_TIMEOUT);
		return defaultContext;
	}
	
	public static String getDefaultBrokerUrl() {
		return DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000);
	}
}

/**
 * 
 */
package com.paic.arch.jmsbroker.context;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;

/**
 * @author zwustudy
 * 配置文件形式JmsContext加载器，因此考虑到此次重构需要满足在测试环境进行确保消息的发收正常的测试，同时需要在所有应用程序进行消息传递，
 * 因此这里有两种环境的切换。如果在所有应用程序间进行消息传递，那么意味着需要统一的端口和IP地址，考虑到部署在线上，还需要用户名和密码限制。
 * 这里如果提供了有效配置文件，那么就按照配置文件中的信息生成JmsMessageBroker;否则就按照测试环境进行消息收发测试
 */
public class ConfigContextLoader extends DefaultContextLoader {
	
	private static final Logger LOG = getLogger(ConfigContextLoader.class);

	private static final String DEFAULT_CONTEXT_CONFIG_FILE_NAME = "properties/jmsbroker.properties";
	
	private static final String BROKER_URL_KEY = "broker.url";
	
	private static final String BROKER_USERNAME_KEY = "broker.username";
	
	private static final String BROKER_PASSWORD_KEY = "broker.password";
	
	private static final String BROKER_TIMEOUT_KEY = "broker.timeout";
	
	private String contextConfigFileName;
	
	public ConfigContextLoader() {
		setContextConfigFileName(null);
	}
	
	public ConfigContextLoader(String contextConfigFileName) {
		setContextConfigFileName(contextConfigFileName);
	}

	public void setContextConfigFileName(String contextConfigFileName) {
		LOG.debug("using jms context config file:{}", contextConfigFileName);
		if (contextConfigFileName == null) {
			LOG.warn("input a null contextConfigFileName, using defaultContextConfigFileName:{} instead.", DEFAULT_CONTEXT_CONFIG_FILE_NAME);
			contextConfigFileName = DEFAULT_CONTEXT_CONFIG_FILE_NAME;
		}
		this.contextConfigFileName = contextConfigFileName;
	}
	
	public JmsContext loadContext() {
		
		Properties properties = new Properties();
        InputStream is = null;
		try {
			is = getClass().getClassLoader().getResourceAsStream(contextConfigFileName);
			properties.load(is);
			String brokerUrl = properties.getProperty(BROKER_URL_KEY);
			String username = properties.getProperty(BROKER_USERNAME_KEY);
			String password = properties.getProperty(BROKER_PASSWORD_KEY);
			String timeoutValue = properties.getProperty(BROKER_TIMEOUT_KEY);
			int timeout = null == timeoutValue ? DEFAULT_RECEIVE_TIMEOUT : Integer.parseInt(timeoutValue);
			JmsContext jmsContext = new JmsContext();
			jmsContext.setUrl(brokerUrl);
			jmsContext.setUsername(username);
			jmsContext.setPassword(password);
			jmsContext.setTimeout(timeout);
			return jmsContext;
		} catch (NullPointerException e) {
			LOG.warn("Failed to load context config file: {}, using default jmsContext.");
			return super.loadContext();
		} catch (IOException e) {
			LOG.error("Failed to load jms context from context config file:{}", contextConfigFileName, e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return null;
	}
	
}

/**
 * 
 */
package com.paic.arch.jmsbroker.context;

/**
 * @author zwustudy
 * jms环境的几个常用参数
 */
public class JmsContext {
	
	private String url;
	
	private String username;
	
	private String password;
	
	private int timeout;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}

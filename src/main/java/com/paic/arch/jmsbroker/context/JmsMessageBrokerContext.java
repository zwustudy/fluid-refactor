/**
 * 
 */
package com.paic.arch.jmsbroker.context;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;
import com.paic.arch.jmsbroker.api.JmsMessageBroker;

/**
 * @author zwustudy
 * 由于要遵循OCP, 同时重构的类需要向下兼容API，因此这里继承JmsMessageBrokerSupport
 * 同时考虑到不同JMS厂商，有不同的代理技术实现，这里使用了策略模式；
 * 此类作为策略模式的环境角色，持有一个抽象策略JmsMessageBroker.
 */
public class JmsMessageBrokerContext extends JmsMessageBrokerSupport {
	
	private static final Logger LOG = getLogger(JmsMessageBrokerContext.class);
	
	private JmsMessageBroker jmsMessageBroker; 
	
	public JmsMessageBrokerContext(JmsMessageBroker jmsMessageBroker) throws Exception {
		super(null);
		this.jmsMessageBroker = jmsMessageBroker;
	}
	
	
	public final JmsMessageBrokerContext andThen() {
        return this;
    }
	
	public void startBroker() throws Exception {
		jmsMessageBroker.start();
    }

    public void stopTheRunningBroker() throws Exception {
        if (jmsMessageBroker == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        jmsMessageBroker.stop();
    }
    
    public JmsMessageBrokerContext sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
    	jmsMessageBroker.sendATextMessageToDestinationAt(aDestinationName, aMessageToSend);
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return jmsMessageBroker.retrieveASingleMessageFromTheDestination(aDestinationName);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
    	return jmsMessageBroker.retrieveASingleMessageFromTheDestination(aDestinationName, aTimeout);
    }

    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return jmsMessageBroker.getEnqueuedMessageCountAt(aDestinationName);
    }

    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
    	return jmsMessageBroker.isEmptyQueueAt(aDestinationName);
    }
}

/**
 * 
 */
package com.paic.arch.jmsbroker.api;

import com.paic.arch.jmsbroker.context.JmsContext;

/**
 * @author zwustudy
 * 加载JmsContext的抽象接口
 */
public interface ContextLoader {
	
	int ONE_SECOND = 1000;
	
	int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
	
	JmsContext loadContext();

}

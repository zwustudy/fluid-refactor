/**
 * 
 */
package com.paic.arch.jmsbroker.api;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * @author zwustudy
 * 这个接口被以lambda表达式的形式调用，因此添加一个编译器进行合法性检查的注解@FunctionalInterface
 */
@FunctionalInterface
public interface JmsCallback {
	
	String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;

}

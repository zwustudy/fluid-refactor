/**
 * 
 */
package com.paic.arch.jmsbroker.api;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;

import org.slf4j.Logger;

import com.paic.arch.jmsbroker.context.ConfigContextLoader;
import com.paic.arch.jmsbroker.context.JmsContext;


/**
 * @author zwustudy
 * 针对不同的代理技术，是使用策略模式实现，这个抽象类就是策略模式中的抽象策略角色
 * 实现了两个接口，分别JMS环境相关的接口和JMS消息收发相关的接口
 */
public abstract class JmsMessageBroker implements MessageBroker, JmsContextHandler {
	
	private static final Logger LOG = getLogger(JmsMessageBroker.class);
	
	private JmsContext jmsContext;
	
	public JmsContext getJmsContext() {
		return jmsContext;
	}
	
	public JmsMessageBroker(ContextLoader contextLoader) {
		if (contextLoader == null) {
			contextLoader = new ConfigContextLoader();
		}
		this.jmsContext = contextLoader.loadContext();
	}
	
	protected String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
	
	protected String getBrokerUrl() {
		return jmsContext.getUrl();
	}
}

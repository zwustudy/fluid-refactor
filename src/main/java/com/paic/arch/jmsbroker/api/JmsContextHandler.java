/**
 * 
 */
package com.paic.arch.jmsbroker.api;

/**
 * @author zwustudy
 * 遵循接口隔离原则，这里JmsMessageBroker实现的接口拆分为两部分,这一部分是Jms环境相关的接口方法
 */
public interface JmsContextHandler {
	
	void start() throws Exception ;
	
	void stop() throws Exception ;
	
}

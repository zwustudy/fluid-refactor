/**
 * 
 */
package com.paic.arch.jmsbroker.api;

/**
 * @author zwustudy
 * 遵循接口隔离原则，这里JmsMessageBroker实现的接口拆分为两部分,这一部分是Jms消息收发相关的接口方法
 */
public interface MessageBroker {
	
	void sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend);
	
	String retrieveASingleMessageFromTheDestination(String aDestinationName) throws NoMessageReceivedException;
	
	String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) throws NoMessageReceivedException;
	
	long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;
	
    boolean isEmptyQueueAt(String aDestinationName) throws Exception;
    
}

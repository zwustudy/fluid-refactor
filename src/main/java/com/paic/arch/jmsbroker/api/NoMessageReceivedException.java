/**
 * 
 */
package com.paic.arch.jmsbroker.api;

/**
 * @author zwustudy
 * 从需要重构的类JmsMessageBrokerSupport中提取出来的异常类
 */
public class NoMessageReceivedException extends RuntimeException {
  
	private static final long serialVersionUID = -8334627132589170596L;
	
	public NoMessageReceivedException() {
		super();
	}

	public NoMessageReceivedException(String reason) {
        super(reason);
    }
}

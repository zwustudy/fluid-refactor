/**
 * 
 */
package com.paic.arch.jmsbroker.impl;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import com.paic.arch.jmsbroker.api.ContextLoader;
import com.paic.arch.jmsbroker.api.JmsCallback;
import com.paic.arch.jmsbroker.api.JmsMessageBroker;
import com.paic.arch.jmsbroker.api.NoMessageReceivedException;

/**
 * 以ActiveMQ实现的JmsMessageBroker,具体策略角色中的一种
 * 
 * @author zwustudy
 */
public class ActiveMQJmsMessageBroker extends JmsMessageBroker {
	
	private static final Logger LOG = getLogger(ActiveMQJmsMessageBroker.class);
	
	private BrokerService brokerService;
	
	public ActiveMQJmsMessageBroker(ContextLoader contextLoader) throws Exception {
		super(contextLoader);
		brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(getBrokerUrl());
	}

	@Override
	public void start() throws Exception {
		brokerService.start();
	}

	@Override
	public void stop() throws Exception {
		if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
		brokerService.stop();
        brokerService.waitUntilStopped();
	}

	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
		return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
	}

	@Override
	public void sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) throws NoMessageReceivedException {
		executeCallbackAgainstRemoteBroker(getBrokerUrl(), aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
	}

	@Override
	public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
		return getEnqueuedMessageCountAt(aDestinationName) == 0;
	}
	
	
	private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, getJmsContext().getUrl()));
    }
	
	private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }
	
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return executeCallbackAgainstRemoteBroker(getJmsContext().getUrl(), aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }

	@Override
	public String retrieveASingleMessageFromTheDestination(String aDestinationName) throws NoMessageReceivedException {
		return retrieveASingleMessageFromTheDestination(aDestinationName, ContextLoader.DEFAULT_RECEIVE_TIMEOUT);
	}

}

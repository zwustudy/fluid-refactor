/**
 * 
 */
package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.api.ContextLoader;
import com.paic.arch.jmsbroker.api.JmsMessageBroker;
import com.paic.arch.jmsbroker.api.NoMessageReceivedException;

/**
 * 以其它技术实现的JmsMessageBroker
 * @author zwustudy
 */
public class AnotherJmsMessageBroker extends JmsMessageBroker {
	
	public AnotherJmsMessageBroker () throws Exception {
		this(null);
	}
	
	public AnotherJmsMessageBroker(ContextLoader contextLoader) throws Exception {
		super(contextLoader);
		// TODO Auto-generated method stub
	}

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws NoMessageReceivedException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend)
			throws NoMessageReceivedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmptyQueueAt(String aDestinationName) throws NoMessageReceivedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(String aDestinationName) throws NoMessageReceivedException {
		// TODO Auto-generated method stub
		return null;
	}

}
